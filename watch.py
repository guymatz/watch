#!/usr/bin/python

import pyinotify
import sys,os,re
import yaml
import paramiko

class FSEventHandler(pyinotify.ProcessEvent):

    def __init__(self, conf):
        self.base_dir = conf['src_base']
        self.dest_base = conf['dest_base']
        self.precedence = conf['precedence']
        self.remote_host , _, self.remote_base = self.dest_base.partition(":")
        print("remote_base = %s" % self.remote_base)
        self.remote_port = conf['port']
        self.user = conf['ssh_user']
        self.passwd = conf['ssh_passwd']
        self.ssh_key = conf['ssh_key']
        self.file_types = conf['file_types']

    def filetype_should_be_shipped(self, file):
        ext = os.path.splitext(file)[1][1:]
        print("File = %s, Ext = %s, file_types = %s" % (file, ext, self.file_types))
        return ext in self.file_types

    def get_precedent(self, file):
        file = os.path.normpath(file) # removed ./ from beginning of filename
        product = file.partition(os.path.sep)[0] # product dir, e.g. madewell
        path_to_file = file.partition(os.path.sep)[2] # e.g. website/media/css/NewHP/slideshow960.css
        print("file = %s, product = %s, path_to_file = %s" % (file, product, path_to_file))
        # This bit of magic sorts by values
        for p in sorted(self.precedence, key=self.precedence.get):
            print("p = %s" % p)
            if p == product:
                print("I see product %s" % product)
                return None
            precedent_file = os.path.join(p, path_to_file)
            print("precedent_file = %s" % precedent_file)
            if os.path.isfile(precedent_file):
                return precedent_file
        print("Returning None from get_precedent")
        return None


    def file_has_precedent(self, file):
        print("Checking for precedent for %s" % file)
        if self.get_precedent(file):
            return True
        else:
            print("Returning false from has_precedent")
            return False

    def ship(self,local_file):
        lfile = re.sub(self.base_dir, '', local_file)
        # lfile now has a leading slash, which os.path.join doesn't like
        lfile = lfile[1:]
        rfile = os.path.join(self.remote_base, lfile)
        print("remote_base = %s, rfile = %s, lfile = %s, local_file = %s" % (self.remote_base, rfile, lfile, local_file))
        #return
        self.transport = paramiko.Transport((self.remote_host, self.remote_port))
        if self.ssh_key:
            try:
                key = paramiko.RSAKey.from_private_key_file(self.ssh_key)
                self.transport.connect(username=self.user, pkey=key)
            except Exception, e:
                print("Problem logging in with key: %s" % str(e))
                return
        else:
            try:
                self.transport.connect(username=self.user, password=self.passwd)
            except Exception, e:
                print("Problem logging in with password: %s" % str(e))
                return
        try:
            client = paramiko.SFTPClient.from_transport(self.transport)
        except Exception, e:
            print("Problem setting up sftp client: %s" % str(e))
            return
        try:
            if os.path.isdir(lfile):
                mode = os.stat(lfile)[0]
                print("client.mkdir(%s, %s)" % (rfile, mode))
                client.mkdir(rfile, mode)
            else:
	        if not self.filetype_should_be_shipped(lfile):
	            print("File is not to be shipped")
	            return
	        if self.file_has_precedent(lfile):
	            self.ship(self.get_precedent(lfile))
	            # And continue on . . .
                print("client.put(%s, %s)" % (local_file, rfile))
                #return
                client.put(local_file, rfile)
        except Exception, e:
            print("Problem with sending file %s via sftp: %s" % (file, e.args))
            return
        client.close()
        self.transport.close()

    def process_IN_ACCESS(self, event):
        #print "ACCESS event:", event.pathname
        pass

    def process_IN_ATTRIB(self, event):
        #print "ATTRIB event:", event.pathname
        pass

    def process_IN_CLOSE_NOWRITE(self, event):
        #print "CLOSE_NOWRITE event:", event.pathname
        pass

    def process_IN_CLOSE_WRITE(self, event):
        if os.path.basename(event.pathname).startswith('.'):
            return
        print "CLOSE_WRITE event:", event.pathname
        self.ship(event.pathname)

    def process_IN_CREATE(self, event):
        if os.path.basename(event.pathname).startswith('.'):
            return
        print "CREATE event:", event.pathname
        self.ship(event.pathname)

    def process_IN_DELETE(self, event):
        if os.path.basename(event.pathname).startswith('.'):
            return
        print "DELETE event:", event.pathname

    def process_IN_MODIFY(self, event):
        #print "MODIFY event:", event.pathname
        pass

    def process_IN_OPEN(self, event):
        #print "OPEN event:", event.pathname
        pass


def main():
    if len(sys.argv) < 2:
        print("Usage: %s config_file" % sys.argv[0])
        sys.exit(1)
    config_file = sys.argv[1]
    try:
        config = yaml.load(open(config_file, 'r'))
        #print config
    except Exception, e:
        print("Sorry, love!  Something wrong here: %s" % str(e))
        sys.exit(1)

    # watch manager
    wm = pyinotify.WatchManager()
    wm.add_watch(config['src_base'], pyinotify.ALL_EVENTS, rec=True, auto_add=True)

    # event handler
    required_config = ['src_base','dest_base','ssh_user','ssh_passwd']
    for c in required_config:
        if c not in config:
            print("One of more required params are missing from %s" % config_file)
            sys.exit(1)
    if 'port' not in config:
        config['port'] = 22
    eh = FSEventHandler(config)

    # notifier
    notifier = pyinotify.Notifier(wm, eh)
    notifier.loop()

    #eh.transport.close()

if __name__ == '__main__':
    main()

